package hu.mik.prog4.sampleapp.controller;

import hu.mik.prog4.sampleapp.exception.ClientParseException;
import hu.mik.prog4.sampleapp.model.Client;
import hu.mik.prog4.sampleapp.model.Company;
import hu.mik.prog4.sampleapp.model.Person;
import hu.mik.prog4.sampleapp.service.Calculator;
import hu.mik.prog4.sampleapp.service.CompanyService;
import hu.mik.prog4.sampleapp.service.PersonService;
import hu.mik.prog4.sampleapp.util.IdProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Control{

    private final PersonService personService;
    private final CompanyService companyService;
    private final IdProvider idProvider = IdProvider.getInstance();
    private final Logger log = Logger.getLogger(this.getClass().getName());

    public Control(PersonService personService, CompanyService companyService) {
        this.personService = personService;
        this.companyService = companyService;
    }

    public void start(){
        List<String[]> dummyList = createDummyList();

        /* dummyList.forEach(strings -> {
            Client client = convertFromString(strings);
            new Thread(() -> Control.this.doIt(client)).start();
        });
        */

        //strings -> convertFromString(strings
        //runnable -> new Thread(runnable)
        //thread -> thread.start()
        /*dummyList.stream()
                .map(this::convertFromString)
                .map(client -> (Runnable)()->this.doIt(client))
                .map(Thread::new)
                .forEach(Thread::start);
        */
        //new Thread(() -> Control.this.doIt(client)).start();
        this.createDummyList()
                .stream()
                .map(this::convertFromString)
                .forEach(client -> new Thread(() -> this.doIt(client)).start());

        Calculator multiplyingCalculator = (number1, number2) -> number1 * number2;

        long multiplicationResult = multiplyingCalculator.calculate(2, 3);
        this.log.info("Result of multiplicationResult: "+multiplicationResult);

        Calculator addingCalculator = Long::sum;
        long additionResult = addingCalculator.calculate(9312, 3231);
        this.log.info("Result of additionResult: "+additionResult);
    }
    private List<String[]> createDummyList(){
        List<String[]> dummyList = new ArrayList<>();

        dummyList.add(new String[]{"P", "Nev", "Address01","342423AS" });
        dummyList.add(new String[]{"C", "Cegnev", "Address02","432488293" });
        dummyList.add(new String[]{"P", "Nev2", "Address03","123421AS" });
        dummyList.add(new String[]{"C", "Cegnev2", "Address04","11133322" });
        dummyList.add(new String[]{"P", "Nev3", "Address05","848342AS" });
        dummyList.add(new String[]{"C", "Cegnev3", "Address06","94829491" });

        return dummyList;
    }

    private Client convertFromString(String[] array){
        switch (array[0]){
            case "P":
                return new Person(this.idProvider.nextId(),array[1],array[2],array[3]);
            case "C":
                return new Company(this.idProvider.nextId(),array[1],array[2],array[3]);
            default:
                throw new ClientParseException("Unknown type of Client: "+array);
        }
    }

    private void doIt(Client client){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        if(client instanceof Person){
            this.personService.pay((Person)client);
        } else if(client instanceof Company){
            this.companyService.pay((Company)client);
        } else {
            throw new ClientParseException("Unknown type of Client: "+client);
        }
    }
}
