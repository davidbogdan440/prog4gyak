package hu.mik.prog4.sampleapp.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class Client {

    private Long id;
    private String name;
    private String address;

}
