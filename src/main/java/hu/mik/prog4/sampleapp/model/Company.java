package hu.mik.prog4.sampleapp.model;

import hu.mik.prog4.sampleapp.annotation.MyAnnotation;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Objects;

@Getter

@EqualsAndHashCode(callSuper = true)
public class Company extends Client{

    private String taxNumber;

    public Company(Long id, String name, String address, String taxNumber) {
        super(id, name, address);
        this.taxNumber = taxNumber;
    }

    @Override
    public String toString() {
        return "Company{" +
                "taxNumber='" + taxNumber + '\'' +
                "Id: "+getId()+'}';
    }
}
