package hu.mik.prog4.sampleapp.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Objects;

@Getter
@EqualsAndHashCode(callSuper = true)
public class Person extends Client{

    private String idNumber;

    public Person(Long id, String name, String address, String idNumber) {
        super(id, name, address);
        this.idNumber = idNumber;
    }

    @Override
    public String toString() {
        return "Person{" +
                "idNumber='" + idNumber + '\'' +
                "Id: "+getId()+'}';
    }
}


