package hu.mik.prog4.sampleapp;

import hu.mik.prog4.sampleapp.controller.Control;
import hu.mik.prog4.sampleapp.service.CompanyService;
import hu.mik.prog4.sampleapp.service.PersonService;

public class MainApplication {

    public static void main(String[] args) {
        Control control = new Control(new PersonService(), new CompanyService());
        control.start();
    }
}
