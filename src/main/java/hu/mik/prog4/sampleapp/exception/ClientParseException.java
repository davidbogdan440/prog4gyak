package hu.mik.prog4.sampleapp.exception;

import lombok.ToString;

public class ClientParseException extends RuntimeException{
    public ClientParseException() {
    }

    public ClientParseException(String message) {
        super(message);
    }

    public ClientParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientParseException(Throwable cause) {
        super(cause);
    }

    public ClientParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
