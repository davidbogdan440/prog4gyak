package hu.mik.prog4.sampleapp.service;

import hu.mik.prog4.sampleapp.model.Person;

import java.util.logging.Logger;

public class PersonService implements Service<Person> {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public void pay(Person client) {
        this.log.info(Thread.currentThread()+"]"+"Paid for Person: "+client);
    }

    @Override
    public void receiveService(Person client) {
        this.log.info(Thread.currentThread()+"]"+"Received Service for Person: "+client);
    }
}
