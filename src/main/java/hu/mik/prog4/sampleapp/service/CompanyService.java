package hu.mik.prog4.sampleapp.service;

import hu.mik.prog4.sampleapp.model.Company;

import java.util.logging.Logger;

public class CompanyService implements Service<Company>{

    private final Logger log = Logger.getLogger(this.getClass().getName());

    @Override
    public void pay(Company client) {
        this.log.info(Thread.currentThread()+"]"+"Paid for Company: "+client);

    }

    @Override
    public void receiveService(Company client) {
        this.log.info(Thread.currentThread()+"]"+"Received Service for Company: "+client);
    }
}
