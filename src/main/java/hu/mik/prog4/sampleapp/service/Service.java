package hu.mik.prog4.sampleapp.service;

import hu.mik.prog4.sampleapp.model.Client;

public interface Service<T extends Client> {

    void pay(T client);

    void receiveService(T client);

}
