package hu.mik.prog4.sampleapp.service;

@FunctionalInterface //egyetlen, abstract metódusa lehet az interfacenek
public interface Calculator {

    long calculate(long number1, long number2);
}
